import express from 'express';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import webpack from 'webpack';

const webpackConfig = require('../../../webpack.dev.js');
const config = require('config');
const node_env = config.get('Node').Env;
const router = express.Router();
const compiler = webpack(webpackConfig);

const frontendMiddleware = app => {
    const isDev = node_env === 'development';
    if (isDev) {
        console.log('running in dev mode...');
        app.use(
            webpackDevMiddleware(compiler, {
                hot: true,
                publicPath: webpackConfig.output.publicPath
            })
        );
        app.use(
            webpackHotMiddleware(compiler, {
                path: '/__webpack_hmr',
                heartbeat: 10 * 1000
            })
        );
        return router;
    } else {
        console.log('running in production mode...');
        app.use('/build', express.static('build'));
        return router;
    }
};

export default frontendMiddleware;
