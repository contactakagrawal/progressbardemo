import styled from 'styled-components';

const ProgressBar = styled.div`
	box-sizing: border-box;
	border-radius: 5px;
    height: 20px;
    top:-20px;
    position: relative;
    transition: width 300ms ease-out;
    width: ${props => props.width};
	background: ${props => props.background};
	z-index: -1;
`;
export default ProgressBar;
